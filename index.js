/**
 * Initializes the nickname chosen by the user and stores it in pseudo
 * @author Nicolas
 * @return pseudo
 */
function demandePseudo() {
    pseudo = prompt('Saisissez votre pseudo ! ');
    return pseudo;
}

/**
 * function to know how many vowels is contained in a word
 * @author Nicolas
 * @param {string} mot 
 * @returns number of vowels with the variable called 'count' 
 */
function compteVoyelles(mot) {
    let count = 0;
    let tabVoyelles = ["a", "e", "i", "o", "u", "y"];
    for (i = 0; i < mot.length; i++) {
        if (tabVoyelles.includes(mot[i])) {
            count++;
        }
    }
    return count;
}

/**
 * function to generate random number (0 or 1)
 * @author Nicolas
 * @returns 0 or 1
 */
function pileOrFace() {
    let random = Math.round(Math.random());
    return random;
}

/**
 * function to throw a piece
 * @author Nicolas
 * @returns 'vrai if win or false if wrong or alert with wrong answer if choice != 0 or 1
 */
function lancerPileOrFace() {
    let result = pileOrFace();
    let choice = prompt('Choisi 1 pour PILE et 0 pour FACE !');
    if (choice == result) {
        return 'vrai';
    }
    else if (choice == 1 || choice == 0 && choice != result) {
        return 'faux';
    }
    else {
        alert('Attention on demande de rentrer 0 ou 1');
        return lancerPileOrFace();
    }
}

/**
 * function to play pileOuFace with n chances
 * @author Nicolas
 * @param {string} nom 
 * @param {number} chances 
 * @returns a sentence with the score in n chances (depend of number of vowels in 'nom')
 */
function jeuPileOuFace(nom, chances) {
    let chance = chances;
    let score = 0;


    while (chance != 0) {
        let result = lancerPileOrFace();
        if (result == 'vrai') {
            score++;
            console.log(score);
        }
        else if (result == 'faux') {
            chance--;
        }
    }

    let phrase = 'Bravo ' + nom + ' avec ' + chances + ' chances vous avez gagné ' + score + ' fois !';
    console.log('phrase : ' + phrase);
    return score;
}

/**
 * principal function to play
 * @author Nicolas
 * @returns return true if player want to stop the game.
 */
function main() {
    let pseudo = prompt('Quel est votre nom ?');
    let chances = compteVoyelles(pseudo);
    console.log('chances' + chances);
    let score = jeuPileOuFace(pseudo, chances);
    let meilleurScore = score;
    console.log('meilleur score main ' + meilleurScore);
    let arretPartie = false;

    while (arretPartie != true) {
        let rejouez = prompt('Voulez vous rejouez ? Entrez oui ou non');
        if (rejouez == 'oui') {
            score = jeuPileOuFace(pseudo, chances);
            if (score >= meilleurScore) {
                meilleurScore = score;
                console.log('meilleur score if' + meilleurScore);
            }
        }
        else if (rejouez == 'non') {
            arretPartie = true;
            console.log('Sur toutes vos parties le meilleur score était de : ' + meilleurScore);
            return arretPartie;
        }
    }
    return true;
}

