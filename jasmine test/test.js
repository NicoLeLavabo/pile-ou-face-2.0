/**
 * function to know how many vowels is contained in a word
 * @author Nicolas
 * @param {string} mot 
 * @returns number of vowels with the variable called 'count' 
 */
function compteVoyelles(mot) {
    let count = 0;
    let tabVoyelles = ["a", "e", "i", "o", "u", "y"];
    for (i = 0; i < mot.length; i++) {
        if (tabVoyelles.includes(mot[i])) {
            count++;
        }
    }
    return count;
}

/**
 * function to generate random number (0 or 1)
 * @author Nicolas
 * @returns 0 or 1
 */
function pileOrFace() {
    let pile;
    let random = Math.round(Math.random());
    if (random === 0) {
        pile = 0;
        return pile;
    }
    else {
        pile = 1;
        return pile;
    }
}

//test compte Voyelles
describe("la fonction compteVoyelles compte les voyelles dans un mot", function () {
    it("le mot 'aeiou' renvoie 3", function () {
        expect(compteVoyelles('aeiou')).toBe(5);
    });
    it("le mot 'qsdfjklm' renvoie 0", function () {
        expect(compteVoyelles('qsdfjklm')).toBe(0);
    });
    it("le mot 'azerty' renvoie 3", function () {
        expect(compteVoyelles('azerty')).toBe(3);
    });
});

//test pile or face
describe("la fonction pileOrFace retourne 0 ou 1", function () {
    it("", function () {
        let result = pileOrFace();
        expect(result == 0 ||result == 1).toBe(true);
    });
});